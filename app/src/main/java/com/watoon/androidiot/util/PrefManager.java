package com.watoon.androidiot.util;

import android.content.Context;
import android.content.SharedPreferences;

import com.watoon.androidiot.app.App;

/**
 * Created by asepahmad
 * Date: 5/17/19
 * Project: indiscub
 */
public class PrefManager {
    public static final String KEY_PREF = "iot_ph";
    private static final String KEY_KALIBRASI = "kalibrasi_mode";
    private static final String KEY_KINCIR = "kincir_mode";
    private static SharedPreferences mSharedPreferences;

    public static void setModeKalibrasi(boolean mode) {
        mSharedPreferences = App.getInstance().getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_KALIBRASI, mode);
        editor.apply();
    }

    public static Boolean getModeKalibrasi() {
        mSharedPreferences = App.getInstance().getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);
        return mSharedPreferences.getBoolean(KEY_KALIBRASI, false);
    }

    public static void setModeKincir(boolean mode) {
        mSharedPreferences = App.getInstance().getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putBoolean(KEY_KINCIR, mode);
        editor.apply();
    }

    public static Boolean getModeKincir() {
        mSharedPreferences = App.getInstance().getSharedPreferences(KEY_PREF, Context.MODE_PRIVATE);
        return mSharedPreferences.getBoolean(KEY_KINCIR, false);
    }

}
