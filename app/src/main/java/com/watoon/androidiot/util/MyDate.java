package com.watoon.androidiot.util;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by GrT on 6/18/2017.
 */

// Class untuk menyimpan data tanggal
public class MyDate implements Serializable
{
    /**
     *
     */
    private static final long serialVersionUID = 2658470743110043895L;
    
    String[] bulans =
            {
                    "Jan",
                    "Feb",
                    "Mar",
                    "Apr",
                    "Mei",
                    "Jun",
                    "Jul",
                    "Agu",
                    "Sep",
                    "Okt",
                    "Nov",
                    "Des"
            };
    
    static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static final DateFormat dfDate = new SimpleDateFormat("yyyy-MM-dd");
    static final DateFormat dfTime = new SimpleDateFormat("HH:mm");
    
    private Calendar cal;
    
    public static MyDate fromSqlDate(String s)
    {
        MyDate res = new MyDate(s, dfDate);
        return res;
    }

    public static MyDate fromSqlDateTime(String s)
    {
        MyDate res = new MyDate(s);
        return res;
    }
    
    public MyDate()
    {
        cal = Calendar.getInstance();
    }
    
    public MyDate(Date date)
    {
        cal = Calendar.getInstance();
        cal.setTime(date);
    }
    
    public MyDate(String date)
    {
        Date mDate;
        
        try
        {
            mDate = df.parse(date);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
            
            mDate = new Date();
        }
    
        cal = Calendar.getInstance();
        cal.setTime(mDate);
    }
    
    private MyDate(String s, DateFormat f)
    {
        Date mDate;
        
        try
        {
            mDate = f.parse(s);
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        
            mDate = new Date();
        }
    
        cal = Calendar.getInstance();
        cal.setTime(mDate);
    }
    
    public String toSqlDate()
    {
        return dfDate.format(cal.getTime());
    }
    
    public String toSqlDateTime()
    {
        return df.format(cal.getTime());
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public String toString()
    {
        int d = cal.getTime().getDate();
        int m = cal.getTime().getMonth();
        int y = cal.getTime().getYear() + 1900;
        
        return d + " " + getBulan(m) + " " + y;
    }
    
    public String toFormat(String format)
    {
        DateFormat f = new SimpleDateFormat(format);
        return f.format(cal.getTime());
    }
    
    public String toStringWithTime()
    {
        return toString() + " " + dfTime.format(cal.getTime());
    }
    
    public String toTime()
    {
        return dfTime.format(cal.getTime());
    }
    
    private String getBulan(int i)
    {
        return bulans[i];
    }
    
    public static String getCurDate()
    {
        Date d = new Date();
        return dfDate.format(d);
    }
    
    public static String getCurTime()
    {
        Date d = new Date();
        return dfTime.format(d);
    }

    public int getCurTimes()
    {
        Date d = new Date();
        return cal.getTime().getDate();
    }
    
    @SuppressWarnings("deprecation")
    public int getDay()
    {
        return cal.getTime().getDate();
    }
    
    @SuppressWarnings("deprecation")
    public int getMonth()
    {
        return cal.getTime().getMonth();
    }
    
    @SuppressWarnings("deprecation")
    public int getYear()
    {
        return cal.getTime().getYear() + 1900;
    }
    
    @SuppressWarnings("deprecation")
    public int getHour()
    {
        return cal.getTime().getHours();
    }
    
    @SuppressWarnings("deprecation")
    public int getMinute()
    {
        return cal.getTime().getMinutes();
    }
    
    public void setDay(int i)
    {
        cal.set(Calendar.DAY_OF_MONTH, i);
    }
    
    @SuppressWarnings("deprecation")
    public void setMonth(int i)
    {
        cal.set(Calendar.MONTH, i);
    }
    
    @SuppressWarnings("deprecation")
    public void setYear(int i)
    {
        cal.set(Calendar.YEAR, i);
    }
    
    public int compareTo(MyDate d)
    {
        return cal.getTime().compareTo(d.cal.getTime());
    }
}
