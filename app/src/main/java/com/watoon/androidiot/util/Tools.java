package com.watoon.androidiot.util;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

/**
 * Created by asepahmad on 11/27/18.
 */

public class Tools {
    public static String getDeviceID(Context context) {
        String deviceID = Build.SERIAL;
        if (deviceID == null || deviceID.trim().isEmpty() || deviceID.equals("unknown")) {
            try {
                deviceID = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
            } catch (Exception e) {
            }
        }
        return deviceID;
    }
}
