package com.watoon.androidiot.app;

import androidx.multidex.MultiDexApplication;


/**
 * Created by Asep Ahmad S on 3/1/2018.
 */

public class App extends MultiDexApplication {
    static App mInstance;

    public static synchronized App getInstance(){
        return mInstance;
    }

    public void setmInstance(App instance){
        App.mInstance = instance;
    }

    @Override
    public void onCreate(){
        super.onCreate();
        setmInstance(this);

    }

}
