package com.watoon.androidiot.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LimitLine;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.watoon.androidiot.R;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Observable;
import rx.schedulers.Schedulers;

public class GraphSuhuActivity extends AppCompatActivity {

    private static final float TOTAL_MEMORY = 100.0f;
    private static final float LIMIT_MAX_MEMORY = 70.0f;
    private FirebaseDatabase dbFire;
    private DatabaseReference databaseReference, refPH;
    float iPH = 0f;

    @BindView(R.id.chart)
    LineChart mChart;
    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
    List<String> nowTime = new ArrayList<>();
    private int iData = 0;
    private float nTemp = 0f;

    public static void go(Context _context) {
        Intent i = new Intent(_context, GraphSuhuActivity.class);
        _context.startActivity(i);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph_suhu);
        ButterKnife.bind(this);

        initView();
    }

    private void initView() {
        dbFire = FirebaseDatabase.getInstance();
        databaseReference = dbFire.getReference();
        refPH = databaseReference.child("temp");

        refPH.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String temp = dataSnapshot.getValue().toString();
                nTemp = Float.parseFloat(temp);


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        setupChart();
        setupAxes();
        setupData();
        setLegend();

        Observable.timer( 1000, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.io())
                .repeat()
                .map(o -> {
                    addHum(nTemp);
                    return o;
                })
                .subscribe();
    }

    private void setLegend() {
        Legend l = mChart.getLegend();

        // modify the legend ...
        l.setForm(Legend.LegendForm.CIRCLE);
        l.setTextColor(Color.WHITE);
    }

    private void setupData() {
        LineData data = new LineData();
        data.setValueTextColor(Color.WHITE);

        // add empty data
        mChart.setData(data);
    }

    private void setupAxes() {
        XAxis xl = mChart.getXAxis();
        xl.setTextColor(Color.WHITE);
        xl.setDrawGridLines(true);
        xl.setAvoidFirstLastClipping(true);
        xl.setEnabled(true);
        xl.setPosition(XAxis.XAxisPosition.BOTTOM);
        xl.setValueFormatter(new TimeFormatter(mChart));

        YAxis leftAxis = mChart.getAxisLeft();
        leftAxis.setTextColor(Color.WHITE);
        leftAxis.setAxisMaximum(TOTAL_MEMORY);
        leftAxis.setAxisMinimum(0f);
        leftAxis.setDrawGridLines(true);

        YAxis rightAxis = mChart.getAxisRight();
        rightAxis.setEnabled(false);
    }

    private void setupChart() {
        // disable description text
        mChart.getDescription().setEnabled(false);
        // enable touch gestures
        mChart.setTouchEnabled(true);
        // if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(true);
        // enable scaling
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        // set an alternative background color
        mChart.setBackgroundColor(Color.DKGRAY);
    }

    private LineDataSet createSet() {
        LineDataSet set = new LineDataSet(null, "SUHU");
        set.setAxisDependency(YAxis.AxisDependency.LEFT);
        set.setColors(ColorTemplate.VORDIPLOM_COLORS[0]);
        set.setCircleColor(Color.WHITE);
        set.setLineWidth(2f);
        set.setCircleRadius(4f);
        set.setValueTextColor(Color.WHITE);
        set.setValueTextSize(10f);
        // To show values of each point
        set.setDrawValues(true);
        return set;

    }


    private void addHum(Float hum) {
        LineData data = mChart.getData();
        nowTime.add(formatter.format(new Date(System.currentTimeMillis())));

        if (data != null) {
            ILineDataSet set = data.getDataSetByIndex(0);

            if (set == null) {
                set = createSet();
                data.addDataSet(set);
            }

            data.addEntry(new Entry(iData, hum), 0);

            // let the chart know it's data has changed
            data.notifyDataChanged();
            mChart.notifyDataSetChanged();

            // limit the number of visible entries
            mChart.setVisibleXRangeMaximum(10);

            // move to the latest entry
            mChart.moveViewToX(data.getEntryCount());

            iData++;
        }
    }

    public class TimeFormatter extends ValueFormatter {

        private final BarLineChartBase<?> chart;

        public TimeFormatter(BarLineChartBase<?> chart) {
            this.chart = chart;
        }

        @Override
        public String getAxisLabel(float value, AxisBase axis) {
            int in = (int)value;
            if (in < 0)
                return "";
            else
                return nowTime.get(in);
        }

        //        @Override
//        public String getFormattedValue(float value) {
//                if(value<0)
//                    return "";
//                else
//                    return nowTime.get(Math.round(value-1));
//            }
//        }
    }


}
