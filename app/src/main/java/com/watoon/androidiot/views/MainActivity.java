package com.watoon.androidiot.views;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;
import com.watoon.androidiot.R;
import com.watoon.androidiot.models.UserDevice;
import com.watoon.androidiot.util.Tools;

public class MainActivity extends AppCompatActivity {

    private TextView txtSuhu, txtKelembaban;
    private FirebaseDatabase dbFire;
    private DatabaseReference databaseReference, refTemp, refHum,refDevice;
    private Button btnGrph;
    private String hum,temp;
    private UserDevice modelDevice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbFire = FirebaseDatabase.getInstance();
        txtSuhu = (TextView)findViewById(R.id.temp);
        txtKelembaban = (TextView)findViewById(R.id.hum);

        databaseReference = dbFire.getReference();
        refTemp = databaseReference.child("temp");
        refHum = databaseReference.child("hum");

        setDeviceInfo();

        btnGrph = (Button)findViewById(R.id.btnGrph);
        btnGrph.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this,GraphActivity.class);
                i.putExtra("suhu",temp);
                i.putExtra("hum",hum);
                startActivity(i);
            }
        });
    }

    private void setDeviceInfo(){
        modelDevice = new UserDevice();
        modelDevice.id = Tools.getDeviceID(this);
        modelDevice.token = FirebaseInstanceId.getInstance().getToken();

        refDevice = dbFire.getReference("UserDevice");
        refDevice.child(Tools.getDeviceID(this)).child("token").setValue(FirebaseInstanceId.getInstance().getToken());

    }

    @Override
    protected void onStart() {
        super.onStart();

        refTemp.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                temp = dataSnapshot.getValue().toString();
                txtSuhu.setText(temp+" degC");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        refHum.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                hum = dataSnapshot.getValue().toString();
                txtKelembaban.setText(hum+" %");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });


    }
}
