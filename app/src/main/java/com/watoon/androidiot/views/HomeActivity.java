package com.watoon.androidiot.views;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;

import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.CookieManager;
import android.webkit.URLUtil;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.ArcProgress;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;
import com.suke.widget.SwitchButton;
import com.watoon.androidiot.R;
import com.watoon.androidiot.common.Constant;
import com.watoon.androidiot.models.NotifModel;
import com.watoon.androidiot.services.fcm.NotifService;
import com.watoon.androidiot.util.PermissionHandler;
import com.watoon.androidiot.util.PrefManager;

import java.text.DecimalFormat;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity implements PermissionHandler.PermissionCallback{
    private static final String TAG = HomeActivity.class.getSimpleName();
    @BindView(R.id.tool_bar)
    Toolbar toolbar;
    @BindView(R.id.txtPH)
    TextView txtPH;
    @BindView(R.id.txtSuhu)
    TextView txtSuhu;
    @BindView(R.id.imgPh)
    ImageView imgPh;
    @BindView(R.id.imgSuhu)
    ImageView imgSuhu;
    @BindView(R.id.imgKincir)
    ImageView imgKincir;
    @BindView(R.id.tStatPh)
    TextView tStatPh;
    @BindView(R.id.tStatSuhu)
    TextView tStatSuhu;
    @BindView(R.id.tStatKincir)
    TextView tStatKincir;
    @BindView(R.id.txtStatus)
    TextView txtStatus;
    @BindView(R.id.ph_progress)
    ArcProgress phProgress;
    @BindView(R.id.suhu_progress)
    ArcProgress suhuProgress;
    @BindView(R.id.btnDownload)
    Button btnDownload;
    @BindView(R.id.web_view)
    WebView webView;
    @BindView(R.id.loader)
    ProgressBar loader;
    @BindView(R.id.btnPH)
    View btnPH;
    @BindView(R.id.btnSuhu)
    View btnSuhu;
    @BindView(R.id.swKalibrasi)
    SwitchButton swKalibrasi;
    @BindView(R.id.swKincir)
    SwitchButton swKincir;
    @BindView(R.id.tvModeKincir)
    TextView tvModkincir;
    @BindView(R.id.tvModeKalibrasi)
    TextView tvModKalibrasi;

    private FirebaseDatabase dbFire;
    private DatabaseReference databaseReference, refTemp, refPH, refKincir, refModeKincir, refKal;
    private String pH = "0";
    private String temp="0";
    private String valKincir="0";
    private Timer timer;
    private Timer timerPH;
    private static DecimalFormat df = new DecimalFormat("#.#");
    private String status = "Mengambil Data";
    private int statColor;
    private NotifService.NotifHandler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
        initView();
        swKalibrasi.setChecked(PrefManager.getModeKalibrasi());
        swKincir.setChecked(PrefManager.getModeKincir());
        tvModKalibrasi.setText(PrefManager.getModeKalibrasi() ? "Mode Kalibrasi (ON)":"Mode Kalibrasi (OFF)");
        tvModkincir.setText(PrefManager.getModeKincir() ? "Mode Kincir (Manual)":"Mode Kincir (Auto)");
        imgKincir.setEnabled(PrefManager.getModeKincir());


    }

    @SuppressLint("SetTextI18n")
    private void initView(){
        toolbar.setTitleTextColor(Color.WHITE);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(getResources().getString(R.string.app_name));

        dbFire = FirebaseDatabase.getInstance();
        databaseReference = dbFire.getReference();
        refTemp = databaseReference.child("temp");
        refPH = databaseReference.child("ph");
        refKincir = databaseReference.child("kincir");
        refKal = databaseReference.child("kalibrasi");
        refModeKincir = databaseReference.child("mode_kincir");
        suhuProgress.setSuffixText((char) 176 + " C");
        phProgress.setSuffixText("");
        phProgress.setMax(14);

        setDeviceInfo();
        retriveData();

        imgKincir.setOnClickListener(v -> {
            if(valKincir.equals("1")){
                refKincir.setValue(0);
            }else refKincir.setValue(1);
        });
        btnPH.setOnClickListener(v -> GraphPHActivity.go(this));
        btnSuhu.setOnClickListener(v -> GraphSuhuActivity.go(this));
        btnDownload.setOnClickListener(v -> {
            if (PermissionHandler.checkPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE))
                startDownload();
            else
                PermissionHandler.requestMultiPermission(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, PermissionHandler.REQUEST_CODE_CHOOSE);
        });

        //webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.setWebViewClient(new WebViewClient());
        webView.setDownloadListener((url, userAgent, contentDisposition, mimetype, contentLength) -> {
            final String data_file = URLUtil.guessFileName(url, contentDisposition, mimetype);
            final AlertDialog.Builder builder = new AlertDialog.Builder(HomeActivity.this);
            builder.setTitle("Download");
            builder.setMessage("Anda ingin mengunduh "+data_file+" ?");
            builder.setPositiveButton("Ya", (dialog, which) -> {
                DownloadManager.Request request = new DownloadManager.Request(Uri.parse(url));
                DownloadManager DM = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
                String cookies = CookieManager.getInstance().getCookie(url);
                request.setMimeType(mimetype);
                request.addRequestHeader("cookies", cookies);
                request.addRequestHeader("uerAgent", userAgent);
                request.setTitle(data_file);
                request.setAllowedOverRoaming(false);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI|DownloadManager.Request.NETWORK_MOBILE);
                request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                request.allowScanningByMediaScanner();
                request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, data_file);
                if (DM != null) {
                    DM.enqueue(request);
                }
                Toast.makeText(getApplicationContext(), "Downloading File", Toast.LENGTH_LONG).show();

            });
            builder.setNegativeButton("Tidak", (dialog, which) -> dialog.cancel());
            AlertDialog dialog = builder.create();
            dialog.show();
            if(dialog.isShowing())showLoader(false);
        });

        swKalibrasi.setOnCheckedChangeListener((view, isChecked) -> {
            PrefManager.setModeKalibrasi(isChecked);
            tvModKalibrasi.setText(isChecked ? "Mode Kalibrasi (ON)":"Mode Kalibrasi (OFF)");
            refKal.setValue(isChecked);
        });
        swKincir.setOnCheckedChangeListener((view, isChecked) -> {
            if(isChecked){
                PrefManager.setModeKincir(true);
                refModeKincir.setValue(1);
                tvModkincir.setText("Mode Kincir (Manual)");
            }else {
                PrefManager.setModeKincir(false);
                refModeKincir.setValue(0);
                tvModkincir.setText("Mode Kincir (Auto)");
            }
            imgKincir.setEnabled(isChecked);
        });

    }

    private void startDownload() {
        showLoader(true);
        webView.loadUrl("https://watoon.000webhostapp.com/iot/api/export");
    }

    private void setDeviceInfo(){
        FirebaseMessaging.getInstance().subscribeToTopic("iot");

    }

    private void getSuhuProgress(){
        int currentLevelSuhu = (int) Float.parseFloat(temp);
        timer = new Timer();
        final int finalLevel = currentLevelSuhu;
        try {
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    runOnUiThread(() -> {

                        if (suhuProgress.getProgress() == finalLevel) {
                            suhuProgress.setProgress(finalLevel);
                            timer.cancel();
                        } else if(suhuProgress.getProgress() > finalLevel){
                            suhuProgress.setProgress(suhuProgress.getProgress() - 1);
                        }else {
                            suhuProgress.setProgress(suhuProgress.getProgress() + 1);
                        }


                    });
                }
            }, 1000, finalLevel);
        }catch (Exception e){
            e.printStackTrace();
            suhuProgress.setProgress(0);
        }

    }

    private void getPHProgress(){
        int currentLevelPH = (int) Float.parseFloat(pH);
        if(currentLevelPH <= 14){
            timerPH = new Timer();
            final int finalLevel = currentLevelPH;
            try{
                timerPH.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(() -> {
                            if (phProgress.getProgress() == finalLevel) {
                                phProgress.setProgress(finalLevel);
                                timerPH.cancel();
                            } else if(phProgress.getProgress() > finalLevel){
                                phProgress.setProgress(phProgress.getProgress() - 1);
                            }else {
                                phProgress.setProgress(phProgress.getProgress() + 1);
                            }
                        });
                    }
                }, 1000, finalLevel);
            }catch (Exception e){
                e.printStackTrace();
                phProgress.setProgress(0);
            }
        }

    }

    private void retriveData(){
        refTemp.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    setTmp(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                    getSuhuProgress();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        refPH.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    pH = Objects.requireNonNull(dataSnapshot.getValue()).toString();
                    setHum(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                    setStatus(false, dataSnapshot.getValue().toString());
                    getPHProgress();
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });

        refKincir.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    setKincir(Objects.requireNonNull(dataSnapshot.getValue()).toString());
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });
        refKal.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    setStatus(Boolean.parseBoolean(Objects.requireNonNull(dataSnapshot.getValue()).toString()), pH);
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }

        });


    }

    private void setStatus(boolean kalibrasi, String v){
        if(!kalibrasi){
            float ph = Float.parseFloat(v);
            if(ph >= 9.0){
                status = "Berbahaya";
                statColor = R.color.colorRed;
            }else if(ph >= 8.0){
                status = "Peringatan";
                statColor = R.color.orange;
            }else if(ph >= 6.0){
                status = "Normal";
                statColor = R.color.white;
            }else if(ph >= 4.5){
                status = "Peringatan";
                statColor = R.color.orange;
            }else {
                status = "Berbahaya";
                statColor = R.color.colorRed;
            }
            txtStatus.setText(status);
            txtStatus.setTextColor(ContextCompat.getColor(this, statColor));
        }else{
            txtStatus.setText("Kalibrasi");
            txtStatus.setTextColor(ContextCompat.getColor(this, R.color.white));
        }

    }

    private String setHum(String h){
        pH = df.format(Double.parseDouble(h));
        float v = Float.parseFloat(h);
        txtPH.setText(pH);
        imgPh.setColorFilter(v <= 0 ? ContextCompat.getColor(this, R.color.colorRed) : ContextCompat.getColor(this, R.color.blue) , android.graphics.PorterDuff.Mode.SRC_IN);
        tStatPh.setText(v <= 0 ? "Tidak Terbaca":"Terbaca");
        return pH;
    }

    @SuppressLint("SetTextI18n")
    private String setTmp(String t){
        temp = t;
        float v = Float.parseFloat(t);
        txtSuhu.setText(temp+ (char) 176 + " C");
        imgSuhu.setColorFilter(v <= 0 ? ContextCompat.getColor(this, R.color.colorRed) : ContextCompat.getColor(this, R.color.green) , android.graphics.PorterDuff.Mode.SRC_IN);
        tStatSuhu.setText(v <= 0 ? "Tidak Terbaca":"Terbaca");
        return temp;
    }

    private void setKincir(String val){
        imgKincir.setColorFilter(val.equals("1") ? ContextCompat.getColor(this, R.color.orange) : ContextCompat.getColor(this, R.color.colorRed) , android.graphics.PorterDuff.Mode.SRC_IN);
        tStatKincir.setText(val.equals("1") ? "Hidup" : "Mati");
        valKincir = val;

    }

    private void showLoader(boolean show){
        loader.setVisibility(show ? View.VISIBLE:View.GONE);
        btnDownload.setEnabled(!show);
    }

    @Override
    public void Granted() {
        startDownload();
    }

    @Override
    public void Refused(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        startService(new Intent(new NotifModel().status_kode = Constant.ACTION_HIDE_NOTIF, null, this, NotifService.class));
    }
}
