package com.watoon.androidiot.services.fcm;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;
import com.watoon.androidiot.R;
import com.watoon.androidiot.app.App;
import com.watoon.androidiot.common.Constant;
import com.watoon.androidiot.models.NotifModel;
import com.watoon.androidiot.util.PrefManager;
import com.watoon.androidiot.views.HomeActivity;

import org.json.JSONObject;

public class FcmMessagingService extends FirebaseMessagingService {
    private String TAG = FcmMessagingService.class.getSimpleName();
    NotifModel notifModel = new NotifModel();
    private String CHANNEL_ID;
    private String CHANNEL_NAME;
    private static MediaPlayer player;
    private NotifService notifService;

    @Override
    public void onCreate() {
        super.onCreate();
        notifService = new NotifService();
    }

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        if (remoteMessage.getData().size() > 0) {
            JSONObject data = new JSONObject(remoteMessage.getData());
            Log.d("Notifikasi FCM", data.toString());
            notifModel = new Gson().fromJson(data.toString(), NotifModel.class);
            CHANNEL_ID = "my_channel_01";
            CHANNEL_NAME = getString(R.string.channel_name);
            if(!PrefManager.getModeKalibrasi()){
                startService(new Intent(data.toString(), null, this, NotifService.class));

            }

        }


    }

    private void showNotif(NotifModel data){
        Uri defaultUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        createChannel();
        String title = data.title;
        String konten = data.content;
        int ic_status;
        if("NORMAL".equalsIgnoreCase(data.title)){
            ic_status = R.drawable.ic_normal;
            player = MediaPlayer.create(App.getInstance(), Uri.parse("android.resource://com.watoon.androidiot/"+R.raw.sound_normal));
        }else if("PERINGATAN".equalsIgnoreCase(data.title)){
            ic_status = R.drawable.ic_warning;
            player = MediaPlayer.create(App.getInstance(), Uri.parse("android.resource://com.watoon.androidiot/"+R.raw.sound_warning));
        }else if("BERBAHAYA".equalsIgnoreCase(data.title)){
            ic_status = R.drawable.ic_danger;
            player = MediaPlayer.create(App.getInstance(), Uri.parse("android.resource://com.watoon.androidiot/"+R.raw.sound_danger));
        }else {
            ic_status = R.mipmap.ic_launcher_round;
        }

        player.setLooping(true);


        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        Notification mNotification = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotification = new Notification.Builder(this, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), ic_status))
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setStyle(new Notification.BigTextStyle()
                            .bigText(konten))
                    .setContentText(konten).build();
        } else {
            mNotification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), ic_status))
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentTitle(title)
                    .setContentText(konten).build();

        }
        // mNotificationId is a unique int for each notification that you must define
        startSound(mNotification);
        notificationManager.notify(1, mNotification);
    }

    private void createChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setShowBadge(true);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.BLUE);
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{500, 500, 500, 500, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(mChannel);
        }
    }

    public void hideNotif(Notification notification){
        if(player!=null)player.stop();
        stopForeground(true);
    }

    void startSound(Notification notification){
        if(player!=null)player.start();
        startForeground(1,notification);
    }

}
