package com.watoon.androidiot.services.fcm;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.speech.tts.TextToSpeech;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.gson.Gson;
import com.watoon.androidiot.R;
import com.watoon.androidiot.app.App;
import com.watoon.androidiot.common.Constant;
import com.watoon.androidiot.models.NotifModel;
import com.watoon.androidiot.views.HomeActivity;

import java.util.List;
import java.util.Locale;

public class NotifService extends Service implements TextToSpeech.OnInitListener {
    private final static String TAG = NotifService.class.getSimpleName();
    private Notification mNotification;
    private MediaPlayer player;
    private String CHANNEL_ID = "my_channel_01";
    private String CHANNEL_NAME;
    private Looper serviceLooper;
    private NotifHandler serviceHandler;
    private boolean alert;
    private String str;
    private TextToSpeech mTts;


    public NotifService(){}

    @Override
    public void onInit(int status) {
        if (status == TextToSpeech.SUCCESS) {
            int result = mTts.setLanguage(new Locale("id", "ID"));
            if (result == TextToSpeech.LANG_MISSING_DATA ||
                    result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Log.v(TAG, "Language is not available.");
            }
        } else {
            Log.v(TAG, "Could not initialize TextToSpeech.");
        }
    }

    private void sayJarvis(String str) {
        mTts.speak(str.replace(".", ","),
                TextToSpeech.QUEUE_FLUSH,
                null);
    }

    public class NotifHandler extends Handler {

        private MediaPlayer player;
        private Notification notif;
        private NotifModel notifModel;

        public NotifHandler(Looper looper) {
            super(looper);
        }

        public NotifHandler() {

        }

        @Override
        public void handleMessage(Message msg) {
            try {
                Intent intent = (Intent)msg.obj;
                notifModel = new Gson().fromJson(intent.getAction(), NotifModel.class);
                if (notifModel.status_kode.equals(Constant.ACTION_BERBAHAYA)) {
                    showNotif(notifModel);
                    return;
                } else if (notifModel.status_kode.equals(Constant.ACTION_WARNING)) {
                    showNotif(notifModel);
                    return;
                }else if (notifModel.status_kode.equals(Constant.ACTION_NORMAL) ) {
                    showNotif(notifModel);
                    return;
                }
            } catch (Exception e) {
                e.printStackTrace();
                hideNotif();
            }
        }
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("NotifService");
        thread.start();
        serviceLooper = thread.getLooper();
        serviceHandler = new NotifHandler(serviceLooper);
        CHANNEL_NAME = App.getInstance().getString(R.string.channel_name);
        mTts = new TextToSpeech(this,
                this  // OnInitListener
        );
        mTts.setSpeechRate(0.5f);
        str ="Service sudah berjalan ";
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("notifservice",": start");
        Message msg = Message.obtain();
        msg.obj = intent;
        serviceHandler.sendMessage(msg);
        return Service.START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        Log.d("notifservice",": taskremoved");
        startService(new Intent(getApplicationContext(), NotifService.class));

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mTts != null) {
            mTts.stop();
            mTts.shutdown();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    private void showNotif(NotifModel data){
        hideNotif();
        createChannel();
        String title = data.title;
        String konten = data.content;
        int ic_status;
        if("NORMAL".equalsIgnoreCase(data.title)){
            alert = false;
            ic_status = R.drawable.ic_normal;
        }else if("PERINGATAN".equalsIgnoreCase(data.title)){
            alert = true;
            ic_status = R.drawable.ic_warning;
            player = MediaPlayer.create(App.getInstance(), Uri.parse("android.resource://com.watoon.androidiot/"+R.raw.sound_warning));
        }else if("BERBAHAYA".equalsIgnoreCase(data.title)){
            alert = true;
            ic_status = R.drawable.ic_danger;
            player = MediaPlayer.create(App.getInstance(), Uri.parse("android.resource://com.watoon.androidiot/"+R.raw.sound_danger));
        }else {
            ic_status = R.mipmap.ic_launcher_round;
        }

        player.setLooping(true);


        Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(App.getInstance(), 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationManager notificationManager = ((NotificationManager) getSystemService(NOTIFICATION_SERVICE));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotification = new Notification.Builder(this, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), ic_status))
                    .setAutoCancel(true)
                    .setContentTitle(title)
                    .setStyle(new Notification.BigTextStyle()
                            .bigText(konten))
                    .setContentText(konten).build();
        } else {
            mNotification = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.ic_logo)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), ic_status))
                    .setAutoCancel(true)
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentTitle(title)
                    .setContentText(konten).build();

        }
        // mNotificationId is a unique int for each notification that you must define
        //startSound(mNotification);
        //sayJarvis("P H saat ini bernilai "+ data.ph + ", dan suhu bernilai "+ data.temp+ " derajat selsius."+" Status "+data.title);
        sayJarvis(" STATUS "+data.title);
        new Handler().postDelayed(() -> {
           startSound(mNotification);
        }, 3000);
        notificationManager.notify(1, mNotification);
    }

    private void createChannel(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH);
            mChannel.setShowBadge(true);
            mChannel.enableLights(true);
            mChannel.setLightColor(Color.BLUE);
            mChannel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            mChannel.enableVibration(true);
            mChannel.setVibrationPattern(new long[]{500, 500, 500, 500, 500, 400, 300, 200, 400});
            notificationManager.createNotificationChannel(mChannel);
        }
    }

    public void hideNotif(){
        if(player!=null)player.stop();
        stopForeground(true);
    }

    void startSound(Notification notification){
        if(player!=null && alert){
            player.start();
            startForeground(1,notification);
        }
    }
    private boolean isAppIsInBackground(Context context) {
        boolean isInBackground = true;
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT_WATCH) {
            List<ActivityManager.RunningAppProcessInfo> runningProcesses = am.getRunningAppProcesses();
            for (ActivityManager.RunningAppProcessInfo processInfo : runningProcesses) {
                if (processInfo.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND) {
                    for (String activeProcess : processInfo.pkgList) {
                        if (activeProcess.equals(context.getPackageName())) {
                            isInBackground = false;
                        }
                    }
                }
            }
        } else {
            List<ActivityManager.RunningTaskInfo> taskInfo = am.getRunningTasks(1);
            ComponentName componentInfo = taskInfo.get(0).topActivity;
            if (componentInfo.getPackageName().equals(context.getPackageName())) {
                isInBackground = false;
            }
        }

        return isInBackground;
    }

}
