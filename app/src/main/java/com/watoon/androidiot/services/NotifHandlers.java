package com.watoon.androidiot.services;

import android.app.Notification;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import com.google.gson.Gson;
import com.watoon.androidiot.common.Constant;
import com.watoon.androidiot.models.NotifModel;

public class NotifHandlers extends Handler {

    private MediaPlayer player;
    private Notification mNotification;
    private NotifModel notifModel;

    public NotifHandlers(Looper looper) {
        super(looper);
    }

    @Override
    public void handleMessage(Message msg) {
        try {
            Intent intent = (Intent) msg.obj;
            notifModel = new Gson().fromJson(intent.toString(), NotifModel.class);
            if (notifModel.aksi.equals(Constant.ACTION_BERBAHAYA)) {
                //showNotification(key_notif,title_notif,konten_notif,txtintent_notif);
                return;
            } else if (notifModel.aksi.equals(Constant.ACTION_WARNING)) {
                //showNotification(key_order,title_on,konten_order,txtintent_notif);
                return;
            }else if (notifModel.aksi.equals(Constant.ACTION_NORMAL)) {
                //hideNotification();
                return;
            }else if (notifModel.aksi.equals(Constant.ACTION_HIDE_NOTIF)) {
                //hideNotif();
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
