package com.watoon.androidiot.models;

/**
 * Created by Asep Ahmad S on 5/8/2018.
 */

public class Stat {

    private float temp;
    private float hum;

    public float getTemp() {
        return temp;
    }

    public void setTemp(float temp) {
        this.temp = temp;
    }

    public float getHum() {
        return hum;
    }

    public void setHum(float hum) {
        this.hum = hum;
    }
}
