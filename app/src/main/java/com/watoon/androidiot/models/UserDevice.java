package com.watoon.androidiot.models;

import java.io.Serializable;

/**
 * Created by asepahmad on 11/27/18.
 */

public class UserDevice implements Serializable{
    public String id;
    public String token;
}
