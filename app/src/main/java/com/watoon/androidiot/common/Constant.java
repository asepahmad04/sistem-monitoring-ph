package com.watoon.androidiot.common;

/**
 * Created by Asep Ahmad S on 5/9/2018.
 */

public class Constant {
    public static String ACTION_BERBAHAYA = "1";
    public static String ACTION_WARNING = "2";
    public static String ACTION_NORMAL= "3";
    public static String ACTION_HIDE_NOTIF = "hide_notif";

}
