'use strict';

const functions = require('firebase-functions');

const admin = require('firebase-admin');
admin.initializeApp();
var nCode = 0;
var x = 0;

exports.sendNotification = functions.database.ref()
	.onUpdate(( change,context) => {
		const snapshot = change.after;
		var ph = snapshot.val().ph;
		var temperature = snapshot.val().temp;
		var status = "";

            if(ph >= 9.0){
              status = "BERBAHAYA"
              nCode = 1;
            }else if(ph >= 8.0){
              status = "PERINGATAN"
              nCode = 2;
            }else if(ph >= 6.0){
              status = "NORMAL"
              nCode = 3;
            }else if(ph >= 4.5){
              status = "PERINGATAN"
              nCode = 2;
            }else {
              status = "BERBAHAYA"
              nCode = 1;
            }

		const payload = {
        data: {
          title: status,
          content: "Suhu: "+temperature+", pH:"+ph,
          ph: ph.toString(),
          temp: temperature.toString(),
          status_kode: nCode.toString(),
          keterangan: status
        }
      };

      if(x !== nCode && ph > 0){
      	  admin.messaging().sendToTopic("iot",payload)
    		.then(function(response){
         		console.log('Notification sent successfully:'+ph,response);
         		x = nCode;
         		return ph;
    		}) 
    		.catch(function(error){
         		console.log('Notification sent failed:',error);
    		});
    	}
      
 
});
